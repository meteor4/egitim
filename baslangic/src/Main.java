import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        /*int butce = 5300;
        List<String> isimListesi = new ArrayList<String>();
        isimListesi.add("yaşar");
        isimListesi.add("emir");
        isimListesi.add("ethem");
        isimListesi.add("burak");
        isimListesi.add("şahin");
        isimListesi.add("semih");
        isimListesi.add("selda");
        isimListesi.add("buse");
        isimListesi.add("mehmet");

        forObjectDeneme(isimListesi);*/

        //yasVeIsimYazdir();
        //yeniForYapisi();
        tryCatchDeneme();

     /*   if (butce >= 5000 && butce <= 7000) {
            System.out.println("1- ");
            System.out.println("cpu: 2");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");

            System.out.println("2- ");
            System.out.println("cpu: 3");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");

            System.out.println("3- ");
            System.out.println("cpu: 4");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");
        } else if (butce >= 5000 && butce <= 7000) {
            System.out.println("1- ");
            System.out.println("cpu: 2");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");

            System.out.println("2- ");
            System.out.println("cpu: 2");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");

            System.out.println("3- ");
            System.out.println("cpu: 2");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");
        } else if (butce >= 9000) {
            System.out.println("1- ");
            System.out.println("cpu: 4");
            System.out.println("ram: 4");
            System.out.println("ekran: 4");

            System.out.println("2- ");
            System.out.println("cpu: 2");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");

            System.out.println("3- ");
            System.out.println("cpu: 2");
            System.out.println("ram: 2");
            System.out.println("ekran: 2");
        } else {
            System.out.println("girlen değer 5 binve üsütü demelisin");
        }*/




       /* if (butce >= 5000 && butce <= 7000) {
            urunOzellikleri("1", "2", "2", "1");
            urunOzellikleri("2", "1", "1", "2");
            urunOzellikleri("2", "2", "dahili", "3");
        } else if (butce > 7000 && butce <= 9000) {
            urunOzellikleri("2", "2", "2", "1");
            urunOzellikleri("3", "2", "2", "2");
            urunOzellikleri("2", "3", "1", "3");
        } else if (butce > 9000) {
            urunOzellikleri("4", "4", "2", "1");
            urunOzellikleri("4", "4", "4", "2");
            urunOzellikleri("6", "8", "4", "3");
        } else {
            System.out.println("girlen değer 5 binve üsütü demelisin");
        }*/

        //forDeneme(3);

    }


    private static void urunOzellikleri(String cpu, String ram, String ekran, String sira) {
        System.out.println(sira + "-");
        System.out.println("cpu: " + cpu);
        System.out.println("ram: " + ram);
        System.out.println("ekran: " + ekran);
    }


    private static void forDeneme(int sayi) {
        for (int mevcutSayi = 0; mevcutSayi <= sayi; mevcutSayi++) {
            System.out.println(mevcutSayi);
        }
    }


    private static void forObjectDeneme(List<String> isimListesi) {
        for (String isim : isimListesi) {
            System.out.println(isim);
        }
    }

    private static void yasVeIsimYazdir() {
        for (String isim : olusturIsimList()) {
            for (int yas : olusturYasList()) {
                System.out.println("isim: " + isim + " yas:" + yas);
            }
        }
    }

    private static List<String> olusturIsimList() {
        List<String> isimler = new ArrayList<>();
        isimler.add("ahmet");
        isimler.add("ali");
        isimler.add("veli");
        isimler.add("isimsiz");
        return isimler;
    }

    private static List<Integer> olusturYasList() {
        List<Integer> yaslar = new ArrayList<>();
        yaslar.add(10);
        yaslar.add(20);
        yaslar.add(30);
        yaslar.add(40);
        yaslar.add(50);
        yaslar.add(60);
        yaslar.add(70);
        return yaslar;
    }

    private static void yeniForYapisi() {
        //olusturIsimList().forEach(isim -> System.out.println(isim));

        /*for (String isim : olusturIsimList()) {
            if("veli".equals(isim)) {
                System.out.println(isim);
            }
        }*/

        //olusturIsimList().stream().filter(isim -> "veli".equals(isim)).forEach(isim -> System.out.println(isim));

        List<String> newIsimList = olusturIsimList().stream().filter(isim -> "veli".equals(isim)).collect(Collectors.toList());

        olusturIsimList()
                .stream()
                .filter(isim -> "veli".equals(isim) || "ahmet".equals(isim))
                .forEach(isim -> {
                    System.out.println(isim);
                    if ("veli".equals(isim)) {
                        isim = isim + " soyad";
                        System.out.println(isim);
                    }
                });
    }

    private static void tryCatchDeneme() {
        try {
            String deneme = null;
            tryCatchDeneme2();
            tryCatchDeneme3();
            tryCatchDeneme4();
            tryCatchDeneme5();
            tryCatchDeneme6();
            tryCatchDeneme7();
            System.out.println("isim logu");
            System.out.println("hata vermedi için mutluyum");
            System.out.println("hata denemesi yapıyorum");
            System.out.println("hata almadım.");
        } catch (IOException | NullPointerException e) {
            System.out.println(" bu bir "+ e.getMessage());
        } catch (Exception e) {
            if(e instanceof FileNotFoundException) {
                System.out.println("file hatasu aldım "+ e.getMessage());
            }
            System.out.println("uygulama hata aldı "+ e.getMessage());
        } finally {
            System.out.println("finally");
        }
    }

    private static void tryCatchDeneme2() throws Exception {
        try {
            String deneme = "null";
            if (deneme.equals("isim")) {
                System.out.println("metod 2 ");
            }
        } catch (Exception e) {
            throw new Exception(" deneme null olamaz");
        }

    }

    private static void tryCatchDeneme3() throws IOException {
        try {
            String deneme = null;
            if (deneme.equals("isim")) {
                System.out.println("metod 2 ");
            }
        } catch (Exception e) {
            throw new IOException(" ioexception");
        }
    }

    private static void tryCatchDeneme4() throws FileNotFoundException {
        try {
            String deneme = null;
            if (deneme.equals("isim")) {
                System.out.println("metod 2 ");
            }
        } catch (Exception e) {
            throw new FileNotFoundException(" FileNotFoundException");
        }
    }

    private static void tryCatchDeneme5() {
        System.out.println("metod 5 ");
    }

    private static void tryCatchDeneme6() {
        System.out.println("metod 6 ");
    }

    private static void tryCatchDeneme7() {
        System.out.println("metod 7 ");
    }

}